#!/bin/bash

while [ $# -gt 0 ]; do
  case "$1" in
    --name=*)
      name="${1#*=}"
      ;;
    --dist=*)
      dist="${1#*=}"
      ;;
    --release=*)
      release="${1#*=}"
      ;;
    --container_id=*)
      container_id="${1#*=}"
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done

sudo lxc-create -t download -n $name -- --dist $dist --release $release --arch amd64 #--keyserver hkp://keyserver.ubuntu.com
sudo lxc-start $name
sudo lxc-attach -n $name -- bash -c "$(wget https://gitlab.com/pesol/ansible-prerequisites-installer/-/raw/main/install.sh -O -)"

if [ -n "$container_id" ]; then
  sudo lxc-attach -n $name -- bash -c "$(wget https://gitlab.com/pesol/ansible-prerequisites-installer/-/raw/main/set_ip.sh -O-)" '' --container_id=$container_id
  sudo lxc-stop $name
  sudo lxc-start $name
fi
